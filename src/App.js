import { useEffect, useState } from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import About from './components/About'
import AddTask from './components/AddTask'
import Footer from './components/Footer'
import Header from './components/Header'
import TaskDetails from './components/TaskDetails'
import Tasks from './components/Tasks'

export const BASEURL = 'http://localhost:5000/tasks'

function App() {
  const [showAddTask, setShowAddTask] = useState(false)
  const [tasks, setTasks] = useState([])

  useEffect(() => {
    const getTasks = async () => {
      const tasksFromServer = await fetchTasks()
      setTasks(tasksFromServer)
    }

    getTasks()
  }, [])

  const fetchTasks = async () => {
    const res = await fetch(`${BASEURL}`)
    const data = await res.json()

    return data
  }
  const fetchTask = async id => {
    const res = await fetch(`${BASEURL}/${id}`)
    const data = await res.json()

    return data
  }

  // Add task
  const addTask = async task => {
    const res = await fetch(`${BASEURL}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(task),
    })
    const newTask = await res.json()

    setTasks([...tasks, newTask])
    // const id = Math.floor(Math.random() * 10000) + 1
    // const newTask = { id, ...task }
    // setTasks([...tasks, newTask])
  }

  // Delete Task
  const deleteTask = async id => {
    await fetch(`${BASEURL}/${id}`, {
      method: 'DELETE',
    })
    setTasks(tasks.filter(task => task.id !== id))
  }

  // Toggle reminder
  const toggleReminder = async id => {
    const tasktoToggle = await fetchTask(id)
    const updatedTask = { ...tasktoToggle, reminder: !tasktoToggle.reminder }

    const res = await fetch(`${BASEURL}/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(updatedTask),
    })

    const data = await res.json()

    setTasks(tasks.map(task => (task.id === id ? { ...task, reminder: data.reminder } : task)))
  }

  return (
    <Router>
      <div className="container">
        <Header onAdd={() => setShowAddTask(!showAddTask)} showAdd={showAddTask} />

        <Routes>
          <Route
            path="/"
            element={<>
              {showAddTask && <AddTask onAdd={addTask} />}
              {tasks.length > 0 ? <Tasks tasks={tasks} onDelete={deleteTask} onToggle={toggleReminder} /> : 'No Tasks To Show'}
            </>}
          />
          <Route path="/about" element={<About />} />
          <Route path="/task/:id" element={<TaskDetails />} />
        </Routes>
        <Footer />
      </div>
    </Router>
  )
}

export default App
