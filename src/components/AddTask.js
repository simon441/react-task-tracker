import { useState } from 'react'

const AddTask = ({ onAdd }) => {
  const [text, setText] = useState('')
  const [day, setDay] = useState('')
  const [reminder, setReminder] = useState(false)

  const onSubmit = e => {
    e.preventDefault()
    // validate input
    if (!text) {
      alert('Please add text!')
      return
    }

    // send to App.js
    onAdd({ text, day, reminder })

    // clear the form
    setText('')
    setDay('')
    setReminder(false)
  }

  return (
    <form className="add-form" onSubmit={onSubmit}>
      <div className="form-control">
        <label htmlFor="taskName">Task</label>
        <input id="taskName" type="text" placeholder="Add Task" value={text} onChange={e => setText(e.target.value)} />
      </div>
      <div className="form-control">
        <label htmlFor="taskDayTime">Day &amp; Time</label>
        <input id="taskDayTime" type="text" placeholder="Add Day and Time" value={day} onChange={e => setDay(e.target.value)} />
      </div>
      <div className="form-control form-control-check">
        <label htmlFor="taskReminder">Set Reminder</label>
        <input id="taskReminder" type="checkbox" checked={reminder} value={reminder} onChange={e => setReminder(e.currentTarget.checked)} />
      </div>

      {/* <input type="submit" value="Save Task" className="btn btn-block" /> */}
      <button type="submit" className="btn btn-block">
        Save Task
      </button>
    </form>
  )
}

export default AddTask
