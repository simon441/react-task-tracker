import { useEffect, useState } from "react"
import { Navigate, useLocation, useNavigate, useParams } from "react-router-dom"
import { BASEURL } from "../App"
import Button from "./Button"

const TaskDetails = () => {
    const [loading, setLoading] = useState(true)
    const [task, setTask] = useState({})
    const [error, setError] = useState(null)

    const params = useParams()
    const navigate = useNavigate()
    const { pathname } = useLocation()

    useEffect(() => {
        const fetchTask = async () => {
            const res = await fetch(`${BASEURL}/${params.id}`)
            const data = await res.json()

            if (res.status === 404) {
                navigate('/')
            }

            setTask(data)
            setLoading(false)
        }

        try {
            fetchTask()
        } catch (e) {
            setError(e)
        }
    }, [params, navigate])

    if (error) {
        navigate('/')
    }

    return loading ?
        (<h3>Loading...</h3>)
        : (<div>
            <p>{pathname}</p>
            <h3>{task.text}</h3>
            <p>{task.day}</p>
            <Button onClick={() => {
                navigate(-1)
            }} text="Go Back"
            />
        </div>)

}

export default TaskDetails
